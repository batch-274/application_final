package com.zuitt.wdc044.exceptions;

// This will hold an exception(error) message during the registration (will be use in th register() method of UserController Class).


public class UserException extends Exception {

    public UserException(String message){ super(message); }

}